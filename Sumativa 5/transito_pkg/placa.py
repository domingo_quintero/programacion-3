"""
Creado el 19 de Diciembre de 2020
"""


import re
"""
Se importa para utilizar Expresiones Regulares (RegEx)
"""

def intro():
    """
    Le da la bienvenida al sistema de verificación de placas de Panamá.
    """
    print("SISTEMA DE VERIFICACIÓN DE PLACAS")
    print("-------------------\n")
    plac_a = input("Digite el número de placa a verificar: ")
    return plac_a

def rev_placa(info):
    """
    Se comprueba el tipo de placa
    """
    placam = info.upper()

    if re.match('CP[0-9][0-9][0-9][0-9]',placam ):
        print("\nPlaca: ",placam ," Tipo: Oficial-ACP")
    else:
        if re.match('CC|CH|CD|MI|PH|PE[0-9][0-9][0-9][0-9]',placam ):
            print("\nPlaca: ",placam ," Tipo: Diplomatica-Relaciones Exteriores")
        else:
            if re.match('ADM|MMI|MCD|RCD|RMI|[0-9][0-9][0-9]',placam ):
                print("\nPlaca: ", placam, " Tipo: Diplomatica-Relaciones Exteriores")
            else:
                if re.match('MADM[0-9][0-9]', placam):
                    print("\nPlaca: ", placam, " Tipo: Diplomatica-Relaciones Exteriores")
                else:
                    if re.match('PR[0-9][0-9][0-9][0-9]', placam):
                        print("\nPlaca: ", placam, " Tipo: Servicios Especiales-Prensa")
                    else:
                        if re.match('HP[0-9][0-9][0-9][0-9]', placam):
                            print("\nPlaca: ", placam, " Tipo: Servicios Especiales-Radioaficionado")
                        else:
                            if re.match('D[0-9][0-9][0-9][0-9][0-9]', placam):
                                print("\nPlaca: ", placam, " Tipo: Otro-Demostracion")
                            else:
                                if re.match('E[0-9][0-9][0-9][0-9][0-9]', placam):
                                    print("\nPlaca: ", placam, " Tipo: Otro-Especial")
                                else:
                                    if re.match('M[0-9][0-9][0-9][0-9][0-9]', placam):
                                        print("\nPlaca: ", placam, " Tipo: Particular-Motocicleta")
                                    else:
                                        if re.match('[0-9][0-9][0-9][0-9][0-9][0-9]',placam ):
                                            print("\nPlaca: ",placam ," Tipo: Particular")
                                        else:
                                            if re.match('[A-Z][A-Z][0-9][0-9][0-9][0-9]',placam ):
                                                print("\nPlaca: ",placam ," Tipo: Particular")

class LicensePlate:
    """
    Confecciona y comprueba una placa
    """

    def __init__(self,placa):

        self.placa = placa

    def verify(self):
        """
        Se verifica la longitud de la cadena para determinar si es un numero de placa valido.
        Luego se revisa que tipo de placa es.
        """
        tamano = len(self.placa)
        if tamano == 6:
            rev_placa(self.placa)
        else:
          print('\nLa placa no es valida!!!')